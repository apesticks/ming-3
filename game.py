import pygame, registry
from pygame.locals import *

pygame.init()
fps = pygame.time.Clock()

def run():
    while registry.running:
        fps.tick(60)
        registry.screen.fill((0,0,0))
        events()
        update()
        pygame.display.update()

def events():
    for event in pygame.event.get():
        quit_events(event)
        for instance in registry.instances:
            if hasattr(instance, "events_"):
                instance.events_.update(event)

def quit_events(event):
    if event.type == QUIT:
        registry.running = False
    if event.type == KEYDOWN:
        if event.key == K_ESCAPE:
            registry.running = False

def update():
    for instance in registry.instances:
        instance.update()


"""import executes entire file, this prevents it from
#running the game if you just want to access 1 method
from command line"""
if __name__ == "__main__":
    run()
