from level import Level

class Home(Level):
    def __init__(self):
        self.columns = 2
        self.rows = 2
        self.spawn_point = 0, 0
        self.name = "home"
        super(Home, self).__init__()