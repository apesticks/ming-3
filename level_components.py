import pygame, registry
from component import Component

class LevelGraphics_(Component):
    def draw(self):
        self.line_width = 1
        parent = self.parent

        self.point_list = [
            (parent.moved_x + parent.initial_x, parent.moved_y + parent.initial_y),
            (parent.moved_x + parent.initial_x, parent.moved_y + parent.height + parent.initial_y),
            (parent.moved_x + parent.width + parent.initial_x, parent.moved_y + parent.height + parent.initial_y),
            (parent.moved_x + parent.width + parent.initial_x, parent.moved_y + parent.initial_y),
            (parent.moved_x + parent.initial_x, parent.moved_y + parent.initial_y)
        ]
        parent.rect = pygame.draw.lines(registry.screen, (255,255,0), False, self.point_list, self.line_width)

    def update(self):
        self.draw()

class LevelCollision_(Component):

    def collision(self):
        image_rect = registry.player.image_rect
        x, y = self.parent.moved_x, self.parent.moved_y
        initial_x, initial_y = self.parent.initial_x, self.parent.initial_y
        height, width = self.parent.height, self.parent.width

        if image_rect.left < x + initial_x:
            self.parent.moved_x = image_rect.left - initial_x
        if image_rect.right >= x + width + initial_x:
            self.parent.moved_x = image_rect.right - width - initial_x
        if image_rect.top < y + initial_y:
            self.parent.moved_y = image_rect.top - initial_y
        if image_rect.bottom >= y + height + initial_y:
            self.parent.moved_y = image_rect.bottom - height - initial_y

    def update(self):
        self.collision()


