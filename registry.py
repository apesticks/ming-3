import os, pygame
from player import Player
from level_manager import LevelManager
from home import Home

screen_size = (800,600)
window_position = (790, 250)
os.environ["SDL_VIDEO_WINDOW_POS"] = str(window_position[0]) + "," + str(window_position[1])
screen = pygame.display.set_mode(screen_size, 0, 32)

running = True

levels = [Home()]

player = Player()

instances = [
    player,
    LevelManager(),
]
