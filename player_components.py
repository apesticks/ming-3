import registry, pygame
from pygame.locals import *
from component import Component

class PlayerGraphics_(Component):
    def draw(self):
        self.parent.image_rect = registry.screen.blit(self.parent.image, (self.parent.x, self.parent.y))

    def update(self):
        self.draw()

class PlayerMotion_(Component):
    def move(self):
        events_ = self.parent.events_
        level = registry.active_level
        
        if events_.move_down == True:
            level.moved_y = level.moved_y - self.parent.move_speed
        if events_.move_up == True:
            level.moved_y = level.moved_y + self.parent.move_speed
        if events_.move_right == True:
            level.moved_x = level.moved_x - self.parent.move_speed
        if events_.move_left == True:
            level.moved_x = level.moved_x + self.parent.move_speed

    def update(self):
        self.move()

class PlayerEvents_(Component):
    def init(self):
        self.move_up = False
        self.move_down = False
        self.move_right = False
        self.move_left = False
        
    def update(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LSHIFT: self.parent.move_speed = 5
            if event.key == K_w: self.move_up = True
            if event.key == K_s: self.move_down = True
            if event.key == K_d: self.move_right = True
            if event.key == K_a: self.move_left = True

        if event.type == KEYUP:
            if event.key == K_LSHIFT: self.parent.move_speed = 1
            if event.key == K_w: self.move_up = False
            if event.key == K_s: self.move_down = False
            if event.key == K_d: self.move_right = False
            if event.key == K_a: self.move_left = False