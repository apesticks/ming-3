import registry, pygame
from player_components import *

class Player(object):
    def __init__(self):
        self.move_speed = 1
        self.x = registry.screen_size[0]/2
        self.y = registry.screen_size[1]/2
        self.image = pygame.image.load("player_images/player.png")

        self.init_components()

    def init_components(self):
        self.events_ = PlayerEvents_(self)
        self.graphics_ = PlayerGraphics_(self)
        self.motion_ = PlayerMotion_(self)

    def update(self):
        self.graphics_.update()
        self.motion_.update()
