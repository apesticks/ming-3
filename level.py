from level_components import *

class Level(object):
    def __init__(self):
        #self.columns = 0 T_T 
        #self.rows = 0 REMEMBER WHAT WE DID FOR YOU
        self.tile_size = 40
        self.moved_x = 0
        self.moved_y = 0
        self.initial_x = registry.screen_size[0]/2
        self.initial_y = registry.screen_size[1]/2
        self.width = self.columns * self.tile_size
        self.height = self.rows * self.tile_size

        self.init_components()

    def default_xy(self):
        self.moved_x = 0
        self.moved_y = 0
        self.set_xy(self.spawn_point)

    def set_xy(self, spawn_point):
        #sets the level position to something different
        player_rect = registry.player.image.get_rect()
        tile_size = self.tile_size
        initial_x = self.initial_x
        initial_y = self.initial_y
        self.initial_x = initial_x - (spawn_point[0] * tile_size) - (tile_size - player_rect.width)/2
        self.initial_y = initial_y - (spawn_point[1] * tile_size) - (tile_size - player_rect.height)/2  
        
    def init_components(self):
        self.graphics_ = LevelGraphics_(self)
        self.collision_ = LevelCollision_(self)

    def update(self):
        self.graphics_.update()
        self.collision_.update()