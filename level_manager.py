import registry
from home import Home

class LevelManager(object):
    def __init__(self):
        self.set_active_level("home")

    def set_active_level(self, name):
        for level in registry.levels:
            if level.name == name:
                level.default_xy()
                registry.active_level = level

    def init_components(self):
        pass

    def update(self):
        registry.active_level.update()